require 'capistrano_colors'
require 'bundler/capistrano'
require "rvm/capistrano"
require 'hipchat/capistrano'

Capistrano::Configuration.instance.load do
  # we use assets compilation
  load 'deploy/assets'

  ## COMMON DEFINITIONS

  # ssh settings
  # possible options from here: http://net-ssh.github.io/net-ssh/classes/Net/SSH.html
  ssh_options[:auth_methods]          = ["publickey"]
  ssh_options[:keys]                  = [ENV['SSH_KEY_FILE']] unless ENV['SSH_KEY_FILE'].nil?
  ssh_options[:forward_agent]         = true
  ssh_options[:paranoid]              = false
  ssh_options[:port]                  = 22
  default_run_options[:pty]           = true

  # identification
  set(:user)                          { application }

  # repository
  set(:scm)                           { :git }
  set(:scm_verbose)                   { false }
  set(:deploy_via)                    { :remote_cache }
  set(:copy_exclude)                  { [ '.git' ] }

  set(:repository)                    { "git@bitbucket.org:ikitlab/#{application}.git" }
  set(:branch)                        { "master" }

  # directories
  set(:deploy_to)                     { "/srv/#{application}" }
  set(:shared_children)               { (fetch(:more_shared_children, []) + %w(log tmp/pids public/system public/assets public/uploads)).uniq }

  # other settings
  set(:rails_env)                     { "production" }
  set(:use_sudo)                      { false }
  set(:keep_releases)                 { 10 }
  set(:rake)                          { "RAILS_ENV=#{rails_env} #{fetch(:bundle_cmd, 'bundle')} exec rake --trace" }
  set(:normalize_asset_timestamps)    {false }

  # notifications
  set(:hipchat_token)                 { "384bb1c8d3bfbc0bb387b1e84ab6ff" }
  set(:hipchat_room_name)             { application }
  set(:hipchat_announce)              { false }
  set(:hipchat_color)                 { 'green' }
  set(:hipchat_failed_color)          { 'red' }

  # shared settings
  set(:shared_settings)               { (fetch(:more_shared_settings, []) + %w(database.yml)).uniq }

  # RVM: TODO
  # set :rvm_type, :user
  # set :rvm_ruby_string, 'ruby-2.0.0-p247@teslaos'

  ## STAGES

  # all possible stages
  set(:default_stages)                { %w(ikitlab-dev ikitlab-test ikitlab-stage) }
  set(:stages)                        { (fetch(:additional_stages, []) + fetch(:default_stages, [])).map(&:downcase).uniq }

  # Should define stage tasks as soon as posible, before any option in this
  # file is set
  stages.each do |name|
    desc "Set the target stage to `#{name}'."
    task(name) do
      set :stage,   name.to_sym
      set :domain,  'example.com'
      if fetch(:additional_stages, []).include?(name)
        # load non-default stages from file
        load "config/deploy/#{name}" if file_in_load_path?("config/deploy/#{name}")
      else
        # the other stages settings here
        ## TODO: think about this 01
        set  :domain, "#{application}-01.#{name.gsub(/ikitlab-/, '')}.ikitlab.co"
        role :web,    "#{user}@#{domain}"
        role :app,    "#{user}@#{domain}"
        role :db,     "#{user}@#{domain}", :primary => true
      end
    end
  end

  on :load do
    # The first non option argument
    env = ARGV.detect { |a| a.to_s !~ /\A-/ && a.to_s !~ /=/ }
    if stages.include?(env)
      # Execute the specified stage so that recipes required in stage can contribute to task list
      find_and_execute_task(env) if ARGV.any?{ |option| option =~ /-T|--tasks|-e|--explain/ }
    else
      abort 'Wrong stage specified.'
    end
  end

  ## HOOKS

  # copy secured configuration files from shared/ dir to release
  after "deploy:finalize_update", roles: :app do
    shared_settings.each do |f|
      run <<-CMD
        if [ -f #{shared_path}/config/#{f} ]; then \
          rm -f #{release_path}/config/#{f}; \
          mkdir -p #{File.dirname(File.join(release_path, 'config', f))}; \
          cp -a #{shared_path}/config/#{f} #{release_path}/config/#{f}; \
        else \
          echo '#{f} does not exist!'; \
          false; \
        fi
      CMD
    end
  end

  # Puma config
  set(:puma_cmd)            { "#{fetch(:bundle_cmd, 'bundle')} exec puma" }
  set(:pumactl_cmd)         { "#{fetch(:bundle_cmd, 'bundle')} exec pumactl" }
  set(:puma_state)          { "#{shared_path}/sockets/puma.state" }
  set(:puma_workers)        { 2 }

  set :puma_config_file,    './config/puma.rb'

  after "deploy:finalize_update", roles: :app do
    contents = <<-FILE
      daemonize
      environment           '#{rails_env}'
      directory             '#{release_path}'
      state_path            '#{puma_state}'
      bind                  'unix://#{shared_path}/sockets/puma.sock'
      activate_control_app  'unix://#{shared_path}/sockets/pumactl.sock'

      ## BUG: https://github.com/puma/puma/issues/359
      # stdout_redirect       '#{shared_path}/log/puma.stdout', '#{shared_path}/log/puma.stderr', true

      threads 8, 32
      workers #{puma_workers}
      preload_app!
      on_worker_boot do
        ActiveSupport.on_load(:active_record) do
          ActiveRecord::Base.establish_connection
        end
      end
    FILE

    put(contents, File.expand_path(File.join(release_path, puma_config_file)))
  end

  after 'deploy:stop',    'puma:stop'
  after 'deploy:start',   'puma:start'
  after 'deploy:restart', 'puma:restart'

  namespace :puma do
    desc 'Start puma'
    task :start, roles: :app, on_no_matching_servers: :continue do
      run "cd #{current_path} && #{puma_cmd} -C #{puma_config_file}", :pty => false
    end

    desc 'Stop puma'
    task :stop, roles: :app, on_no_matching_servers: :continue do
      run "cd #{current_path} && #{pumactl_cmd} -S #{puma_state} stop"
    end

    desc 'Restart puma'
    task :restart, roles: :app, on_no_matching_servers: :continue do
      begin
        stop
        run 'sleep 5'
        # run "cd #{current_path} && #{pumactl_cmd} -S #{puma_state} phased-restart"
      rescue => ex
        puts "Failed to stop puma: #{ex}\nAssuming not started."
      end
      start
    end
  end

  # set build version
  after "deploy:create_symlink",  roles: :app do
    run "echo BUILD_VERSION = \\'#{fetch(:revision, 'n/a')}\\' > #{release_path}/config/initializers/version.rb"
  end

  namespace :deploy do
    desc "Load the seed data"
    task :seed do
      run "cd #{release_path}; #{rake} db:seed"
    end
  end

  # triggering callbacks
  after "deploy:update_code", "deploy:migrate"
  after "deploy:update_code", "deploy:seed"
  after "deploy:restart",     "deploy:cleanup"

  namespace :web do
    task :disable, :roles => :app do
      on_rollback { run "rm #{release_path}/public/maintenance.html; exit 0" }
      run "ln -s #{release_path}/public/_maintenance.html #{release_path}/public/maintenance.html; true"
    end
    task :enable, :roles => :app do
      run "rm #{release_path}/public/maintenance.html; true"
    end
  end

end
